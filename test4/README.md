# 2019级软件工程4班

| 姓名   | 学号         | 介绍 | Git账号           |
| ------ | ------------ | ---- | ----------------- |
| 周杰   | 201910111230 | 组长 | @WriteAPoemForYou |
| 叶嘉豪 | 201910211624 | 组员 | @yegezuishuai     |
| 杨镇源 | 201910112325 | 组员 | @zyane            |
| 王图龙 | 201910215119 | 组员 | @Wtl0124          |
| 尹宏伟 | 201910112425 | 组员 | @2060518805       |

## 实验4：生产管理系统实体设计

实验目的

根据生产管理系统中的对象，创建部分表(Table)，构建实体联系图（Entity Relationship Diagram）。

表(table)样例： supplier_part_purchase_setup供应商物料采购配置表

| 描述             | 主键外键 | 类型          | 可以为空 | 说明                                                         |
| ---------------- | -------- | ------------- | -------- | ------------------------------------------------------------ |
| id               | PK       | int           | N        | 自动增加                                                     |
| part_no          | FK       | nvarchar(100) | N        | 物料编号,part表外键                                          |
| min_quantity     |          | int           | N        | 最小采购数量,default=1，最小采购数量                         |
| multiple         |          | money         | N        | 供应倍数,default=1，必须>0                                   |
| min_package      |          | money         | N        | 最小包装数,default=1，必须>0,仅供参考，不参加运算            |
| box_quantity     |          | money         | Y        | 整箱数量,default=1，必须>0,仅供参考，不参加运算              |
| purchase_cycle   |          | int           | N        | 采购周期，单位是天                                           |
| supplier_dept_id | FK       | int           | Y        | 供应商ID,[supplier供应商表]()外键，如果为空，表示适合于所有供应商 |

[ER图mermaid格式](https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram)

最后提交日期:2022-4-9

### 1.生产管理系统表

#### 1.1生产单查询

| 约束 | 1． 生产单查询默认（无条件输入或初始化页面） 为查询所有生产<br/>单的前 15 条<br/>2． 结果页面显示数量为 15 条<br/>3． 生产单显示顺序参考生产单创建时间（降序）<br/>4． 生产状态： 未完成、 已领料、 已上机、 已退料、 已完成。<br/>参考 SQL：<br/>Select *,<br/>(Case<br/>When ms.issum>=mo.amount then ‘已完成’<br/>When ms.rmcount>0 then ‘已退料’<br/>When ms.udcount>0 then ‘已上机’<br/>when ms.dmcount>0 then ‘已领料’<br/>Else ‘未完成’<br/>End)<br/>from makeorder mo<br/>Left outer join (<br/>Select dm.mocode,count(DMID) as dmcount, count(UDID) as<br/>udcount , count(RMID) as rmcount, sum( Amount ) issum from<br/>DramMaterial dm<br/>Left outer join UpDownMachjine um on um.mocode=dm.mocode<br/>Left outer join ReturnMaterial rm on rm.mocode=dm.mocode<br/>Left outer join InStore is on is.mocdoe=dm.mocode<br/>) ms on ms.mocode=mo.mocode |
| ---- | ------------------------------------------------------------ |

#### 1.2机台监控

| 约束 | 机台监控数据搜集<br/>参考 SQL：<br/>Select * form machine ma<br/>Left outer join UpDownMachine udm |
| ---- | ------------------------------------------------------------ |

### 2.学生管理系统数据表
| 字段名           | 主键外键 | 类型          | 可以为空 | 说明           |
| ---------------- | -------- | ------------- | -------- | ---------------|
| id               |     PK   | int           | N        | 学生表自动增加 |
| name             |          | nvarchar(100) | N        | 学生姓名       |
| innerNo          |          | int           | N        | 班内序号       |
| gradeId          |     FK   | int           | N        | 年级           |
| classId          |          | int           | N        | 班级           |
| sex              |          | char          | N        | 性别           |

### 3.图书管理系统读者表

| 描述       | 主键外键 | 类型         | 可以为空 | 说明     |
| ---------- | -------- | ------------ | -------- | -------- |
| readerid   | PK       | int          | N        | 读者编号 |
| name       |          | varchar(10)  | N        | 读者姓名 |
| telephone  |          | varchar(11)  | N        | 联系电话 |
| email      |          | varchar(30)  | N        | 邮箱地址 |
| dept       |          | varchar(20)  | N        | 所在院   |
| right      |          | int          | Y        | 借阅权限 |
| readertype | FK       | int          | N        | 读者类型 |
| demo       |          | varchar(255) | Y        | 说明     |
### 4.班级信息数据表
| 字段名           | 主键外键 | 类型          | 可以为空 | 说明           |
| ---------------- | -------- | ------------- | -------- | ---------------|
| Class_id               |     PK   | char           | N        | 记录班级号 |
| Class_name             |          | nvarchar(16) | N        | 班级全称       |
| Class_department          |          | nvarchar(10)           | N        | 记录班级所在系别       |
| Class_teacherid         |  FK     | char (5)          | N        | 记录本班级班主任号          |



